//Navegacion

const navToggle = document.querySelector(".toggler");
const navMenu= document.querySelector(".nav-menu");

navToggle.addEventListener("click",() => {
    navMenu.classList.toggle("nav-menu_visible")
})
//Fin Navegacion
//Modal

 const openmodal = document.querySelector(".btn-modal");
 const modal = document.querySelector(".modal");
 const close = document.querySelector(".modal-close")
 openmodal.addEventListener('click',(e) => {
     e.preventDefault();
     modal.classList.add("modal-show");
     
 })
 close.addEventListener('click',(e) => {
    e.preventDefault();
    modal.classList.remove("modal-show");
    
})

//Fin Modal
