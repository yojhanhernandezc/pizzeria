<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Reem+Kufi&display=swap" rel="stylesheet">
    <script defer src="js/index.js"></script>

    <title>La Sierra</title>
</head>
<body>
    <!--Header-->
    <header>
        <nav>
            <a class="logo" href="#"><img src="img/logo.png" alt=""></a>
            <button class="toggler" aria-label="Abrir menu">
                <img src="https://img.icons8.com/ios-filled/50/ffffff/menu--v1.png"/>
            </button>
            <ul class="nav-menu">
                <li class="nav-menu-item"><a class="nav-menu-link" href="#menu">Menú</a></li>
                <li class="nav-menu-item"><a class="nav-menu-link" href="#galeria">Galería</a></li>
                <li class="nav-menu-item"><a class="nav-menu-link" href="#contacto">Contacto</a></li>
                
            </ul>
        </nav>
        <section class="header">
            <div>
                <h6>Pizzería la sierra</h6>
                <h2>Fundada el 15 de abril<br>
                    de 1989</h2>
                    <a href="#" class="btn-modal">Ver más</a>   
            </div>
        
        </section>
    </header>
    <!--Fin-Header-->
    <!--Modal-->
    <section class="modal">
    <div class="contenedor-modal ">
        <h2 class="titulo-modal">pizzeria la sierra</h2>
        <p class="modal-parrafo">Los Srs. Peter Thiessen Neufel y Albert Thiessen Friessen, obtuvieron un préstamos y juntándolo con las ganas de sobresalir, de hacer algo diferente y asados en una receta canadiense, deciden establecer lo que hoy en día es PIZZERÍA LA SIERRA</p>
        <a class="modal-close" href="#">cerrar</a>

    </div>
    </section>
    <!--Fin Modal-->
    <!--Menu-->
    <section id="menu" class="contenedor-menu">
        <h2>Nuestro Menú</h2>
        <div class="menu">
            <div class="item_menu">
                <img src="img/pizza1.png" alt="">
                <h2>Pizza Jamón</h2>
                <p>$20.000</p>
            </div>
            <div class="item_menu">
                <img src="img/pizza2.png" alt="">
                <h2>Pizza Verduras</h2>
                <p>$30.000</p>
            </div>
            <div class="item_menu">
                <img src="img/pizza3.png" alt="">
                <h2>Pizza Parma</h2>
                <p>$40.000</p>
            </div>
            
        </div>
    </section>
    <section class="contenedor-gradiente">

    </section>
    <!--fin Menu-->
    <!--Galeria-->
    <div class="galeria" id="galeria">
         <h2>Nuestra Galería</h2>
         <div class="slider">
             <ul>
                 <li><img src="img/pizza-slider-1 (1).jpg" alt=""></li>
                 <li><img src="img/pizza-slider-1 (2).jpg" alt=""></li>
                 <li><img src="img/pizza-slider-1 (3).jpg" alt=""></li>
                 <li><img src="img/pizza-slider-1 (4).jpg" alt=""></li>
             </ul>
         </div>
        
    </div> 
    <!--Fin galeria-->
    <!--contacto-->
    <div class="contacto" id="contacto">
    <?php include("registrar.php"); ?>
        <div class="form" >
            
            <form action="registrar.php" method="post">   
            <input type="text" id="nombres" placeholder="Nombres" name="nombre" required >
            <input type="text" id="apellidos" placeholder="Apellidos" name="apellidos" required >
            <input type="text" id="email" placeholder="Correo Electronico" name="email" required >
            <input type="tel" id="telefono" placeholder="Telefono" name="telefono" required >
            <button type="submit" name="register">Enviar</button>
            </form>
        </div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3199.9307557965967!2d-5.447251085224137!3d36.67616588237255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd0d17726767deff%3A0x61df092631ad0916!2sPizzeria%20La%20Sierra!5e0!3m2!1ses!2sco!4v1637842102884!5m2!1ses!2sco" width="300" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
       
       
    </div>
       
</body>
</html> 
